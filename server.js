// import express
const express = require('express'),
      cors = require('cors'),
      request = require('request');

var router = express.Router();

// Instantiate express
const app = express();
const PORT_NUMBER = process.env.PORT || 3002;
const COUNTRIES_API_URL = process.env.COUNTRIES_API_URL || 'http://ec2-13-229-233-153.ap-southeast-1.compute.amazonaws.com:3000/countries';
var whitelist = ['http://localhost:4200']
var corsOptions = {
  origin: function (origin, callback) {
    console.log(origin);
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}
app.use(cors());
console.log("Port : " +PORT_NUMBER);
console.log("Countries URL API : " + COUNTRIES_API_URL);

app.use(express.static(__dirname + '/public'));

// use the router and 401 anything falling through
app.use('/harmful', router, function (req, res) {
    res.status(200).json({h: 'HAHAHAHA'});
})

app.use((req, res, next)=>{
    console.log("First next !");
    var x  = 1000;
    next();
});

app.use((req, res, next)=>{
    console.log("Second next !");
    var y  = 1000;
    next();
});

app.get('/countries', (req,res,next)=>{
    console.log("Retrieve countries ...");
    request(COUNTRIES_API_URL, function (error, response, body) {
        let countriesJObj = JSON.parse(body);
        res.status(200).json(countriesJObj);
    });
})

app.get('/something', (req, res, next)=>{
    //console.log(req);
    let receivedX = req.query.x;
    console.log(receivedX);
    let returnVal = {
        x: 1,
        y: "test",
        y2: "test2"
    };
    res.status(200).json(returnVal);
    //res.status(500).json({error: "unknown"});
});



app.get('/somethingelse/:custId/:employeeId', (req, res, next)=>{
    console.log(req.params);
    let customeIdVal = req.params.custId;
    let employeeIdVal = req.params.employeeId;
    console.log(employeeId);
    console.log(customeId);
    
    if(customeId < 10){
        res.status(500).json({error:"an error has occurred!"});
    }
    res.status(200).json({employeeId: employeeIdVal, customerId: customeIdVal});
})

app.listen(PORT_NUMBER, ()=>{
    console.log(`App listening on ${PORT_NUMBER}`);
})